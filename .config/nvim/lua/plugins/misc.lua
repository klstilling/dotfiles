return {
    "mfussenegger/nvim-ansible",
    {"folke/which-key.nvim", opts = {} },
	"tpope/vim-fugitive",
    {"numToStr/Comment.nvim", opts = {} },
    {"folke/todo-comments.nvim", opts = {}},
    {"lewis6991/gitsigns.nvim", opts = {}},
	{
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = {},
	},
    "godlygeek/tabular",
}
