return {
	{ "nvim-treesitter/playground", cmd = "TSPlaygroundToggle" },
	{
		"windwp/nvim-ts-autotag",
		ft = {
			"astro",
			"glimmer",
			"handlebars",
			"html",
			"javascript",
			"jsx",
			"markdown",
			"php",
			"rescript",
			"svelte",
			"tsx",
			"typescript",
			"vue",
			"xml",
		},
	},
	{
		"mfussenegger/nvim-treehopper",
		keys = { { "m", mode = { "o", "x" } } },
		config = function()
			vim.cmd([[
           omap     <silent> m :<C-U>lua require('tsht').nodes()<CR>
           xnoremap <silent> m :lua require('tsht').nodes()<CR>
         ]])
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter-context",
		event = "BufReadPre",
		enabled = true,
		opts = { mode = "cursor" },
	},
	{
		"nvim-treesitter/nvim-treesitter",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-refactor",
			-- "HiPhish/nvim-ts-rainbow2",
			"theHamsta/nvim-treesitter-pairs",
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
		end,
		opts = {
			ensure_installed = "all",
			sync_install = false,
			matchup = { enable = true },
			highlight = {
				enable = true,
				disable = function(lang, buf)
					local max_filesize = 100 * 1024 -- 100 KB
					local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
					if ok and stats and stats.size > max_filesize then
						return true
					end
				end,
			},
			query_linter = {
				enable = true,
				use_virtual_text = true,
				lint_events = { "BufWrite", "CursorHold" },
			},
			playground = {
				enable = true,
				disable = {},
				updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
				persist_queries = true, -- Whether the query persists across vim sessions
				keybindings = {
					toggle_query_editor = "o",
					toggle_hl_groups = "i",
					toggle_injected_languages = "t",
					toggle_anonymous_nodes = "a",
					toggle_language_display = "I",
					focus_language = "f",
					unfocus_language = "F",
					update = "R",
					goto_node = "<cr>",
					show_help = "?",
				},
			},
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "gnn",
					node_incremental = "grn",
					scope_incremental = "grc",
					node_decremental = "grm",
				},
			},
			indent = {
				enable = true,
				disable = { "yaml", "python" },
			},
			refactor = {
				-- highlight_current_scope = { enable = true },
				highlight_definitions = {
					enable = true,
					-- Set to false if you have an `updatetime` of ~100.
					clear_on_cursor_move = true,
				},
			},
			-- rainbow = {
			-- 	enable = true,
			-- 	-- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
			-- 	extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
			-- 	max_file_lines = 2000, -- Do not enable for files with more than n lines, int
			-- 	-- colors = {}, -- table of hex strings
			-- 	-- termcolors = {} -- table of colour name strings
			-- },
			pairs = {
				enable = true,
				disable = {},
				highlight_pair_events = { "CursorMoved" }, -- when to highlight the pairs, use {} to deactivate highlighting
				highlight_self = true, -- whether to highlight also the part of the pair under cursor (or only the partner)
				goto_right_end = false, -- whether to go to the end of the right partner or the beginning
				fallback_cmd_normal = "normal! %", -- What command to issue when we can't find a pair (e.g. "normal! %")
				keymaps = {
					goto_partner = "<leader>%",
					delete_balanced = "X",
				},
				delete_balanced = {
					only_on_first_char = false, -- whether to trigger balanced delete when on first character of a pair
					fallback_cmd_normal = nil, -- fallback command when no pair found, can be nil
					longest_partner = false, -- whether to delete the longest or the shortest pair when multiple found.
					-- E.g. whether to delete the angle bracket or whole tag in  <pair> </pair>
				},
			},
			autotag = { enable = true },
		},
	},
}
