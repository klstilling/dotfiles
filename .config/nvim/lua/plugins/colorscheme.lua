return {
	{
		"ellisonleao/gruvbox.nvim",
		config = function(_, opts)
			require("gruvbox").setup(opts)
			vim.cmd.colorscheme("gruvbox")
			vim.opt.background = "dark"
		end,
		opts = {
			terminal_colors = true,
			undercurl = true,
			underline = false,
			bold = true,
			italic = {
				strings = true,
                emphasis = true,
				operators = true,
				comments = true,
                folds = true,
			},
			strikethrough = true,
			invert_selection = false,
			invert_signs = false,
			invert_tabline = false,
			invert_intend_guides = false,
			inverse = true, -- invert background for search, diffs, statuslines and errors
			contrast = "hard", -- can be "hard", "soft" or empty string
			palette_overrides = {},
			overrides = {},
			dim_inactive = false,
			transparent_mode = false,
		},
	},
}
