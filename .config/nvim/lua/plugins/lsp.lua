-- Dictionary additions for LTeX-LS
local path = vim.fn.stdpath("config") .. "/spell/en.utf-8.add"
local words = {}

for word in io.open(path, "r"):lines() do
	table.insert(words, word)
end

local lsp_servers = {
	ansiblels = {},
	bashls = {},
	ts_ls = {
		single_file_support = false,
		settings = {
			typescript = {
				inlayHints = {
					includeInlayParameterNameHints = "literal",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = false,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},
			javascript = {
				inlayHints = {
					includeInlayParameterNameHints = "all",
					includeInlayParameterNameHintsWhenArgumentMatchesName = false,
					includeInlayFunctionParameterTypeHints = true,
					includeInlayVariableTypeHints = true,
					includeInlayPropertyDeclarationTypeHints = true,
					includeInlayFunctionLikeReturnTypeHints = true,
					includeInlayEnumMemberValueHints = true,
				},
			},
		},
	},
	html = {},
	gopls = {
		gofumpt = true,
	},
	cssls = {},
	eslint = {},
	pylsp = {
		settings = {
			pylsp = {
				plugins = {
					-- disabled plugins are provided by ruff!
					autopep8 = { enabled = false },
					flake8 = { enabled = false },
					mccabe = { enabled = false },
					pycodestyle = { enabled = true, ignore = { "E501" } },
					pydocstyle = { enabled = false },
					pyflakes = { enabled = false },
					pylint = { enabled = false },
					ruff = {
						enabled = true,
						extendSelect = { "I", "S", "FIX" },
						format = { "I001" },
						unsafeFixes = true,
						preview = true,
					},
					jedi = { environment = "/usr/bin/python3" },
					-- Rope auto-completer
					rope_autoimport = { enabled = true },
					-- rope_completion = { enabled = true },
					-- jedi_completion = { enabled = false },
				},
			},
		},
	},
	lua_ls = {
		Lua = {
			workspace = { checkThirdParty = false, library = vim.api.nvim_get_runtime_file("", true) },
			telemetry = { enable = false },
			diagnostics = { globals = { "vim" } },
			format = { enable = false },
		},
	},
	ltex = {
		ltex = {
			language = "en-GB",
			dictionary = {
				["en-GB"] = words,
			},
		},
	},
	marksman = {},
}

return {
	{
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		lazy = true,
		dependencies = {
			"williamboman/mason.nvim",
		},
	},
	{
		"neovim/nvim-lspconfig",
		init = function()
			-- disable lsp watcher. Too slow on linux
			local ok, wf = pcall(require, "vim.lsp._watchfiles")
			if ok then
				wf._watchfunc = function()
					return function() end
				end
			end
		end,
		event = { "BufReadPre", "BufNewFile" },
		setup = {},
		dependencies = {
			"williamboman/mason-lspconfig.nvim",
			"onsails/lspkind.nvim", -- pretty icons
		},
	},
	{
		"lvimuser/lsp-inlayhints.nvim",
		branch = "anticonceal",
		event = "LspAttach",
		opts = {},
		config = function(_, opts)
			require("lsp-inlayhints").setup(opts)
			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("LspAttach_inlayhints", {}),
				callback = function(args)
					if not (args.data and args.data.client_id) then
						return
					end
					local client = vim.lsp.get_client_by_id(args.data.client_id)
					require("lsp-inlayhints").on_attach(client, args.buf)
				end,
			})
		end,
	},
	{
		"b0o/SchemaStore.nvim",
		lazy = true,
		config = function(_, _)
			lsp_servers = vim.tbl_extend("force", lsp_servers, {
				yamlls = {
					settings = {
						redhat = { telemetry = { enabled = false } },
						yaml = {
							schemaStore = { enable = false, url = "" },
							schemas = require("schemastore").yaml.schemas({
								replace = {
									-- catalog entries: https://github.com/b0o/SchemaStore.nvim/blob/main/lua/schemastore/catalog.lua
									["Ansible Playbook"] = {
										description = "Ansible playbook files",
										fileMatch = {
											"playbook.yml",
											"playbook.yaml",
											"site.yml",
											"site.yaml",
											"**/playbooks/*.yml",
											"**/playbooks/*.yaml",
											"**/42n/*.yml",
											"**/42n/*.yaml",
										},
										name = "Ansible Playbook",
										url = "https://raw.githubusercontent.com/ansible/ansible-lint/main/src/ansiblelint/schemas/ansible.json#/$defs/playbook",
									},
								},
							}),
							customTags = { "!vault", "!lambda" },
							validate = true,
							completion = true,
							format = { enabled = false }, -- we use yamlfix for formatting
						},
					},
				},
				jsonls = {
					settings = {
						json = {
							schemas = require("schemastore").json.schemas(),
							validate = { enable = true },
						},
					},
				},
			})

			-- print("schemastore config" .. vim.inspect(vim.tbl_keys((Lsp_servers))))
		end,
	},
	{
		"williamboman/mason-lspconfig.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"b0o/SchemaStore.nvim",
			"WhoIsSethDaniel/mason-tool-installer.nvim",
			"stevearc/conform.nvim",
			{
				"williamboman/mason.nvim",
				cmd = { "Mason" },
				build = ":MasonUpdate",
				config = true,
			},
			{ "folke/neodev.nvim", opts = {} },
		},
		opts = function()
			local o = {}
			lsp_servers = vim.tbl_extend("force", lsp_servers, {
				yamlls = {
					redhat = { telemetry = { enabled = false } },
					yaml = {
						schemaStore = { enable = false, url = "" },
						schemas = require("schemastore").yaml.schemas(),
						customTags = { "!vault", "!lambda" },
						validate = true,
						completion = true,
						format = { enabled = false }, -- we use yamlfix for formatting
					},
				},
				jsonls = {
					json = {
						schemas = require("schemastore").json.schemas(),
						validate = { enable = true },
					},
				},
			})

			local installable_tools = vim.tbl_keys(lsp_servers)
			installable_tools = vim.list_extend(installable_tools, {
				"fixjson",
				"golangci-lint",
				"markdownlint",
				"shellcheck",
				"shfmt",
				"sql-formatter",
				"stylua",
				"yamlfix",
			})
			require("mason-tool-installer").setup({
				auto_update = true,
				ensure_installed = installable_tools,
			})

			return o
		end,
		config = function(_, opts)
			-- print("mason-lspconfig config" .. vim.inspect(vim.tbl_keys(Lsp_servers)))
			require("mason-lspconfig").setup(opts)
			-- Diagnostic keymaps
			vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
			vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
			vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
			vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })
			-- Custom diagnostic signs
			local signs = { Error = "❌", Warn = "⚠", Hint = "💡", Info = "ℹ" }
			for type, icon in pairs(signs) do
				local hl = "DiagnosticSign" .. type
				vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
			end

			-- display diagnostics source as virtual text
			vim.lsp.handlers["textDocument/publishDiagnostics"] =
				vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
					virtual_text = {
						source = "always", -- Or "if_many"
					},
				})

			local on_attach = function(_, bufnr)
				local nmap = function(keys, func, desc)
					if desc then
						desc = "LSP: " .. desc
					end

					vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
				end

				local telescope = require("telescope.builtin")

				nmap("<space>rn", vim.lsp.buf.rename, "[R]e[n]ame")
				nmap("<space>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")

				nmap("da", telescope.diagnostics, "[D]iagnosics List [A]ll")
				nmap("gd", telescope.lsp_definitions, "[G]oto [D]efinition")
				nmap("gr", telescope.lsp_references, "[G]oto [R]eferences")
				nmap("gI", telescope.lsp_implementations, "[G]oto [I]mplementation")
				nmap("<space>D", telescope.lsp_type_definitions, "Type [D]efinition")
				nmap("<space>ds", telescope.lsp_document_symbols, "[D]ocument [S]ymbols")
				nmap("<space>ws", telescope.lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")

				-- See `:help K` for why this keymap
				nmap("K", vim.lsp.buf.hover, "Hover Documentation")
				nmap("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")

				-- Lesser used LSP functionality
				nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
				nmap("<space>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
				nmap("<space>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
				nmap("<space>wl", function()
					-- print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
				end, "[W]orkspace [L]ist Folders")

				-- Create a command `:Format` local to the LSP buffer
				vim.api.nvim_create_user_command("Format", function(args)
					local range = nil
					if args.count ~= -1 then
						local end_line = vim.api.nvim_buf_get_lines(0, args.line2 - 1, args.line2, true)[1]
						range = {
							start = { args.line1, 0 },
							["end"] = { args.line2, end_line:len() },
						}
					end
					require("conform").format({ async = true, lsp_format = "fallback", range = range })
				end, { desc = "Format current buffer with Conform, fallback to LSP" })
			end

			-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

			require("mason-lspconfig").setup_handlers({
				function(server_name)
					-- print("mason-lspconfig setup handlers" .. vim.inspect(vim.tbl_keys(Lsp_servers)))
					-- print("Setting up " .. server_name)
					local ls_config = {
						capabilities = capabilities,
						on_attach = on_attach,
						settings = lsp_servers[server_name],
					}

					if server_name == "yamlls" then
						-- print("yamlls config " .. vim.inspect(ls_config))
						ls_config = vim.tbl_deep_extend("force", ls_config, {
							filetypes = vim.tbl_deep_extend(
								"force",
								require("lspconfig.server_configurations.yamlls").default_config.filetypes,
								{ "yaml.ansible" }
							),
						})
					end
					require("lspconfig")[server_name].setup(ls_config)
				end,
			})
		end,
	},
	{
		"stevearc/conform.nvim",
		opts = function()
			return {
				formatters_by_ft = {
					json = { "fixjson" },
					lua = { "stylua" },
					markdown = { "markdownlint" },
					sh = { "shfmt" },
					yaml = { "yamlfix" },
					["*"] = { "injected" },
				},
			}
		end,
		config = function(_, opts)
			require("conform").setup(opts)
			require("conform").formatters.yamlfix = {
				env = {
					YAMLFIX_WHITELINES = 1,
					YAMLFIX_preserve_quotes = "True",
					YAMLFIX_sequence_style = "block_style",
				},
			}
			require("conform").formatters.markdownlint = {
				prepend_args = { "-c", vim.fn.expand("~/.config/markdownlint.json") },
			}
		end,
	},
	{
		"mfussenegger/nvim-lint",
		config = function()
			require("lint").linters.systemd_analyze = {
				name = "systemd-analyze",
				cmd = "systemd-analyze",
				args = { "verify" },
				stdin = false,
				stream = "stderr",
				ignore_exitcode = true,
				parser = require("lint.parser").from_errorformat("%f:%l: %m", {
					source = "systemd_analyze",
					severity = vim.diagnostic.severity.WARN,
				}),
			}
			require("lint").linters.visudo = {
				name = "visudo",
				cmd = "visudo",
				args = { "-cf", "-" },
				stdin = true,
				stream = "stderr",
				ignore_exitcode = true,
				parser = require("lint.parser").from_errorformat("%tarning: %f:%l:%c: %m, %f:%l:%c: %m", {
					source = "visudo",
				}),
			}

			require("lint").linters_by_ft = {
				go = { "golangcilint" },
				yaml = { "yamllint" },
				markdown = { "markdownlint" },
				sh = { "shellcheck" },
				systemd = { "systemd_analyze" },
				sudoers = { "visudo" },
			}
			require("lint").linters.markdownlint.args = {
				"-c",
				vim.fn.expand("~/.config/markdownlint.json"),
			}

			vim.api.nvim_create_autocmd({ "BufReadPost", "BufWritePost", "TextChanged" }, {
				callback = function()
					require("lint").try_lint()
				end,
			})
		end,
	},
	-- Useful status updates for LSP
	{
		"j-hui/fidget.nvim",
		opts = {},
		event = { "LspAttach" },
	},
	-- Lightbulb for Code Actions
	{
		"kosayoda/nvim-lightbulb",
		opts = { autocmd = { enabled = true } },
		event = { "LspAttach" },
		init = function()
			-- Make the bulb use a nerdfont icon instead of emoji
			vim.fn.sign_define("LightBulbSign", { text = "󰌵" })
		end,
	},
	{
		"aznhe21/actions-preview.nvim",
		keys = {
			{
				"<space>ca",
				function()
					require("actions-preview").code_actions()
				end,
				mode = { "n", "v" },
			},
		},
		opts = {
			telescope = {
				sorting_strategy = "ascending",
				layout_strategy = "vertical",
				layout_config = {
					width = 0.8,
					height = 0.9,
					prompt_position = "top",
					preview_cutoff = 20,
					preview_height = function(_, _, max_lines)
						return max_lines - 15
					end,
				},
			},
		},
	},
}
