return {
	{
		"L3MON4D3/LuaSnip",
		build = (not jit.os:find("Windows"))
				and "echo 'NOTE: jsregexp is optional, so not a big deal if it fails to build'; make install_jsregexp"
			or nil,
		dependencies = {
			{
				"rafamadriz/friendly-snippets",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load()
				end,
			},
			{
				url = "https://gitlab.com/klstilling/ansible-snippets.git",
				config = function()
					require("luasnip.loaders.from_vscode").lazy_load()
				end,
			},
		},
		opts = {
			history = true,
			delete_check_events = "TextChanged",
		},
		keys = {
			{
				"<C-e>",
				function()
					return require("luasnip").jumpable(1) and "<Plug>luasnip-jump-next" or "<C-e>"
				end,
				expr = true,
				silent = true,
				mode = "i",
			},
			{
				"<C-n>",
				function()
					require("luasnip").jump(1)
				end,
				mode = "s",
			},
			{
				"<s-tab>",
				function()
					require("luasnip").jump(-1)
				end,
				mode = { "i", "s" },
			},
		},
	},
	-- {
	-- 	"L3MON4D3/LuaSnip",
	-- 	config = function()
	-- 		require("luasnip").config.setup({ history = true })
	--
	-- 		-- require("luasnip/loaders/from_vscode").load({ paths = { "~/.config/nvim/ansible-snippets" } })
	-- 		-- require("luasnip/loaders/from_vscode").load({ paths = { "~/.config/nvim/friendly-snippets" } })
	--
	-- 		vim.api.nvim_set_keymap("s", "<C-e>", "<Plug>luasnip-next-choice", {})
	-- 		vim.api.nvim_set_keymap("i", "<C-n>", "<Plug>luasnip-jump-next", {})
	-- 		vim.api.nvim_set_keymap("s", "<C-n>", "<Plug>luasnip-jump-next", {})
	-- 	end,
	-- },
	-- {
	-- 	"rafamadriz/friendly-snippets",
	-- 	config = function()
	-- 		require("luasnip.loaders.from_vscode").lazy_load()
	-- 	end,
	-- },
}
