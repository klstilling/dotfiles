return {
	"nvim-lualine/lualine.nvim",
	init = function()
		vim.opt.showmode = false -- disable extra modal line under statusline
	end,
	opts = {
		options = {
			extensions = { "fugitive" },
			theme = "auto",
			section_separators = "",
			component_separators = "",
		},
	},
}
