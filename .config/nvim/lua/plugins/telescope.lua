return {
    {
        'stevearc/dressing.nvim',
        opts = {},
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        config = function(_, opts)
            local builtin = require("telescope.builtin")
            vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
            vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
            vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
            vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})
        end,
        opts = {
            defaults = {
                file_ignore_patterns = {
                    "venv/",
                    "git/",
                    "hg/",
                    "svn/",
                    "%.exe",
                    "%.so",
                    "%.dll",
                    "%.class",
                    "%.aux",
                    "%.fls",
                    "%.log",
                    "%.blg",
                    "%.bbl",
                    "%.toc",
                    "%.pdf",
                    "%.out",
                    "%.synctex.tex",
                    "%.synctex.gz",
                    "%.fdb_latexmk",
                    "%.zip",
                    "%.eps",
                    "%.png",
                    "%.pyc",
                    "%.sqlite",
                    "%.PDF",
                    "%.svg",
                    "%.gnuplot",
                    "%.min.js",
                },
            },
            pickers = {},
            extensions = {
                fzf = {
                    fuzzy = true,
                    override_generic_sorter = true,
                    override_file_sorter = true,
                    case_mode = "smart_case",
                },
            },
        },
    },
}
