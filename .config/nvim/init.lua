vim.g.guifont = "monospace"
vim.g.python3_host_prog = "/usr/bin/python3"

vim.opt.expandtab = true -- Use the appropriate number of spaces to insert a <Tab>
vim.opt.tabstop = 4 -- Number of spaces that a <Tab> in the file counts for
vim.opt.shiftwidth = 4 -- Number of spaces to use for each step of (auto)indent
vim.opt.softtabstop = -1 -- a negative value sets to value of shiftwidth

vim.opt.updatetime = 100 -- mseconds between cursorhold updates

vim.opt.ignorecase = true -- ignore case in search patterns
vim.opt.smartcase = true -- override ignorecase if upper case chars in pattern
vim.opt.clipboard = "unnamedplus"
-- vim.opt.textwidth = 120  -- automatically wrap text at 80 characters
-- vim.opt.colorcolumn = '+1'  -- set colorcolumn depending on textwidth
vim.opt.colorcolumn = "80"

vim.opt.termguicolors = true -- enable 24-bit RGB color
vim.opt.number = true -- display number column
vim.opt.relativenumber = true -- number column relative to current line

vim.opt.spelllang = "en_gb"
vim.opt.spell = true

require("config.lazy")

vim.filetype.add({
  extension = {
    j2 = function(path, _)
      -- try to guess filetype
      local root = vim.fn.fnamemodify(path, ":r")
      local base_ft = vim.filetype.match({ filename = root })

      if base_ft == nil then
        -- failed to guess base filetype. Use the extension as filetype instead
        base_ft = vim.split(vim.fn.fnamemodify(path, ":e:e"), ".", { plain = true })[1]
      end

      return base_ft .. ".jinja"
    end,
  },
  pattern = {
    -- Set filetype on GitLab CLI issue notes (comments)
    ["ISSUE_NOTE_EDITMSG.*"] = "markdown",
    ['${HOME}/work/42n/.*.ya?ml'] = "yaml.ansible",
    ['${HOME}/work/zabbix/.*.ya?ml'] = "yaml.ansible",
    ['${HOME}/work/42n/host_vars/.*'] = "yaml.ansible",
  },
})
